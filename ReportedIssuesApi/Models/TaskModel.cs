﻿using System;
using System.Collections.Generic;

namespace ReportedIssuesApi.Models
{
    public class TaskModel
    {

        private int _taskID;
        private string _latitude;
        private string _longitude;
        private string _description;
        private string _taskStatus;
        private string _address;
        private DateTime _date;

        public TaskModel()
        {
            
        }
        public TaskModel(int taskID, string latitude, string longitude, string description, DateTime date, string address = "", string status = "New")
        {
            _taskID = taskID;
            _latitude = latitude;
            _longitude = longitude;
            _description = description;
            _taskStatus = status;
            _address = address;
            _date = date;
        }
        public int TaskID { get { return _taskID; } set { _taskID = value; } }
        public string Lat { get { return _latitude; } set { _latitude = value; } }
        public string Lng { get { return _longitude; } set { _longitude = value; } }
        public string Description { get { return _description; } set { _description = value; } }
        public string Status { get { return _taskStatus; } set { _taskStatus = value; } }
        public string Address { get { return _address; } set { _address = value; } }
        public DateTime TaskDate { get { return _date.Date; } set { _date = DateTime.Today; } }
    }
}