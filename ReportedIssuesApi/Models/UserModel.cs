﻿using System.ComponentModel.DataAnnotations;

namespace ReportedIssuesApi.Models
{
    public class UserModel
    {
        private int _userID;
        private string _firstName;
        private string _LastName;
        private string _role;
        [EmailAddress(ErrorMessage = "Email is invalid"), MaxLength(50)]
        private string _email;

        public UserModel(int userID, string firstName, string lastName, string role, string email)
        {
            _userID = userID;
            _firstName = firstName;
            _LastName = lastName;
            _role = role;
            _email = email;
        }
        public UserModel()
        {

        }
        public int UserID { get { return _userID; } set { _userID = value; } }
        public string FirstName { get { return _firstName; } set { _firstName = value; } }
        public string LastName { get { return _LastName; } set { _LastName = value; } }
        public string Role { get { return _role; } set { _role = value; } }
        [EmailAddress(ErrorMessage = "Email is invalid"), MaxLength(50)]
        public string Email { get { return _email; } set { _email = value; } }
    }
}