﻿namespace ReportedIssuesApi.Models
{
    public class TaskAndOfficerModel
    {
        private int _taskAndOfficerID;
        private int _taskID;
        private int _officerID;

        public TaskAndOfficerModel(int taskAndOfficerID, int taskID, int officerID)
        {
            _taskAndOfficerID = taskAndOfficerID;
            _taskID = taskID;
            _officerID = officerID;
        }
        public TaskAndOfficerModel()
        {

        }

        public int TaskAndOfficerID { get { return _taskAndOfficerID; } set { _taskAndOfficerID = value; } }
        public int TaskID { get { return _taskID; } set { _taskID = value; } }
        public int OfficerID { get { return _officerID; } set { _officerID = value; } }


    }
}