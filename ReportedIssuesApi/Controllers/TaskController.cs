﻿using System.Web.Http;
using ReportedIssuesApi.Repository;
using System.Threading.Tasks;
using ReportedIssuesApi.Models;

namespace ReportedIssuesApi.Controllers
{
    public class TaskController : ApiController
    {
        private readonly TaskRepository _taskRepository = new TaskRepository();

        [Route("api/task")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllTasks()
        {
            var task = await _taskRepository.GetAllTasks();
            if (task != null)
            {
                return Ok(task);
            }
            else
            {
                return BadRequest("No task found");
            }
        }

        [Route("api/task/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetTaskByID(int id)
        {
            var task = await _taskRepository.GetTaskByID(id);
            if (task != null)
            {
                return Ok(task);
            }
            else
            {
                return BadRequest("Task with given id not found");
            }
        }

        [Route("api/task/status/{status}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetTaskByStatus(string status)
        {
            var task = await _taskRepository.GetTaskByStatus(status);
            if (task != null)
            {
                return Ok(task);
            }
            else
            {
                return BadRequest("Task(s) with given status do(es) not exist");
            }
        }

        [Route("api/task")]
        [HttpPost]
        public async Task<IHttpActionResult> InsertTask(TaskModel newTask)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model");
            }

            var result = await _taskRepository.InsertTask(newTask);
            return Ok("New task addes successfully");
        }

        [Route("api/task/{id}")]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateTask(int id, TaskModel updatedTask)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model");
            }

            var result = await _taskRepository.UpdateTask(id, updatedTask);

            if (!result)
            {
                return BadRequest("Task with given id does not exist");
            }

            return Ok("Task updated successfully");
        }

        [Route("api/task/status/{status}")]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateTaskStatus(int id, string status)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid task model");
            }

            var result = await _taskRepository.UpdateStatus(id, status);

            if (!result)
            {
                return BadRequest("Task with given id does not exist");
            }

            return Ok("Task status updated successsfully");

        }

        [Route("api/task/{id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteTask(int id)
        {
            var task = await _taskRepository.DeleteTask(id);

            if (!task)
            {
                return BadRequest("Task with given id does not exist");
            }

            return Ok("Task removed successfully");
            
        }
    }
}