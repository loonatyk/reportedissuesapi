﻿using ReportedIssuesApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using ReportedIssuesApi.Models;

namespace ReportedIssuesApi.Controllers
{
    public class TaskAndOfficerController:ApiController
    {
        private readonly TaskAndOfficerRepository taskAndOfficerRepository = new TaskAndOfficerRepository();


        [Route("api/tao")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllTasks()
        {

            var getAll = await taskAndOfficerRepository.GetAllTaskAndOfficer();

            if (getAll != null)
            {
                return Ok(getAll);
            }

            return BadRequest("No tasks assigned to officers found");
        }
        [Route("api/tao/task/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllByTaskID(int id)
        {

            var getAll = await taskAndOfficerRepository.GetAllByTaskID(id);

            if (getAll != null)
            {
                return Ok(getAll);
            }

            return BadRequest($"Task with ID:{id} have no officer assigned");
        }

        [Route("api/tao/officer/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllByOfficerID(int id)
        {

            var getAll = await taskAndOfficerRepository.GetAllByOfficerID(id);

            if (getAll != null)
            {
                return Ok(getAll);
            }

            return BadRequest($"Officer with ID:{id} have no task assigned");
        }

        [Route("api/tao/new")]
        [HttpPost]
        public async Task<IHttpActionResult> NewTaskAndOfficer(TaskAndOfficerModel taskAndOfficer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model");
            }

            var newTaskAndOfficer = await taskAndOfficerRepository.InsertTaskAndOfficer(taskAndOfficer);

            if (!newTaskAndOfficer)
            {
                return BadRequest("Task whas been not assigned to officer");
            }

            return Ok("Task assigned successflly");
        }

        [Route("api/tao/update")]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateTaskAndOfficer(TaskAndOfficerModel taskAndOfficer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model");
            }

            var update = await taskAndOfficerRepository.UpdateTaskAndOfficer(taskAndOfficer);

            if (!update)
            {
                return BadRequest("Task assignment with given ID does not exist");
            }

            return Ok("Task assignment updated");
        }

        [Route("api/tao/delete/{id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteUser(int id)
        {
            var user = await taskAndOfficerRepository.DeleteTaskAndOfficer(id);
            if (user == true)
            {
                return Ok("TaskAndOfficer removed successfully");
            }
            else
            {
                return BadRequest("TaskAndOfficer with given id does not exit");
            }
        }

    }


}