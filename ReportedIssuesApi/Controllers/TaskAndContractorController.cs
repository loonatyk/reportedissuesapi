﻿using ReportedIssuesApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ReportedIssuesApi.Models;

namespace ReportedIssuesApi.Controllers
{
    public class TaskAndContractorController : ApiController
    {
        private readonly TaskAndContractorRepository taskAndContractorRepo = new TaskAndContractorRepository();
        //private readonly TaskRepository task = new TaskRepository();
        //private readonly ContractorRepository contractor = new ContractorRepository();

        [Route("api/tac")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllTasks()
        {
            
            var getAll = await taskAndContractorRepo.GetAllTaskAndContractor();

            if (getAll != null)
            {
                return Ok(getAll);
            }

            return BadRequest("No tasks assigned to contractors found");
        }

        //dodac sprawdzanie czy taskid i contractorid istnieja dla ponizszych
        [Route("api/tac/task/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllByTaskID(int id)
        {

            var getAll = await taskAndContractorRepo.GetAllByTask(id);

            if (getAll != null)
            {
                return Ok(getAll);
            }

            return BadRequest($"Task with ID:{id} have no contractor assigned");
        }

        [Route("api/tac/contractor/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllByContractorID(int id)
        {

            var getAll = await taskAndContractorRepo.GetAllByContractor(id);

            if (getAll != null)
            {
                return Ok(getAll);
            }

            return BadRequest($"Contractor with ID:{id} have no tasks assigned");
        }

        [Route("api/tac/new")]
        [HttpPost]
        public async Task<IHttpActionResult> NewTaskAndContractor(TaskAndContractorModel taskAndContractor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model");
            }

            var newTaskAndContractor = await taskAndContractorRepo.InsertTaskAndContractor(taskAndContractor);

            if (!newTaskAndContractor)
            {
                return BadRequest("Task whas been not assigned to contractor");
            }

            return Ok("Task assigned successflly");
        }

        [Route("api/tac/update")]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateTaskAndContractor(TaskAndContractorModel taskAndContractor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model");
            }

            var update = await taskAndContractorRepo.UpdateTaskAndContractor(taskAndContractor);

            if (!update)
            {
                return BadRequest("Task assignment with given ID does not exist");
            }

            return Ok("Task assignment updated");
        }

        [Route("api/tac/delete/{id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteUser(int id)
        {
            var user = await taskAndContractorRepo.DeleteTaskAndContractor(id);
            if (user == true)
            {
                return Ok("TaskAndContractor removed successfully");
            }
            else
            {
                return BadRequest("TaskAndContractor with given id does not exit");
            }
        }
    }
}
