﻿using ReportedIssuesApi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportedIssuesApi.Repository
{
    public class ContractorRepository
    {
        // Get all contractors
        public async Task<List<ContractorModel>> GetAllContractors()
        {
            using (var context = new ReportedIssuesEntities())
            {
                var contractors = context.Contractors
                    .Select(x => new ContractorModel()
                    {
                        ContractorID = x.ContractorID,
                        CompanyName = x.CompanyName,
                        LineOfBusiness = x.LineOfBusiness
                    })
                    .ToList<ContractorModel>();

                if (contractors.Count != 0)
                {
                    return contractors;
                }
                else
                {
                    return null;
                }
            }
        }

        // Get contractor by ID
        public async Task<List<ContractorModel>> GetContractorByID(int id)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var contractor = context.Contractors
                    .Where(x => x.ContractorID == id)
                    .Select(x => new ContractorModel()
                    {
                        ContractorID = x.ContractorID,
                        CompanyName = x.CompanyName,
                        LineOfBusiness = x.LineOfBusiness
                    })
                    .ToList<ContractorModel>();

                if (contractor.Count != 0)
                {
                    return contractor;
                }
                else
                {
                    return null;
                }
            }
        }

        Contractor contractor = new Contractor();

        // INSERT Contractor
        public async Task<bool> InsertContractor(ContractorModel contr)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var companyName = context.Contractors.Where(x => x.CompanyName == contr.CompanyName).Count();

                contractor.CompanyName = contr.CompanyName;
                contractor.LineOfBusiness = contr.LineOfBusiness;

                if (contr != null && companyName == 0)
                {
                    context.Contractors.Add(contractor);
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public enum UpdateContractorResult
        {
            ContractorDoesNotExist,
            ContractorAlreadyExists,
            ContractorAddesSuccessfully
        }

        // UPDATE Contractor
        public async Task<UpdateContractorResult> UpdateContractor(ContractorModel contr)
        {
            using(var context = new ReportedIssuesEntities())
            {
                var contractorToBeUpdated = context.Contractors
                    .Where(x => x.ContractorID == contr.ContractorID)
                    .FirstOrDefault<Contractor>();

                var companyName = context.Contractors.Where(x => x.CompanyName == contr.CompanyName).Count();

                if (contractorToBeUpdated == null)
                {
                    return UpdateContractorResult.ContractorDoesNotExist;
                }
                if (companyName != 0)
                {
                    return UpdateContractorResult.ContractorAlreadyExists;
                }

                contractorToBeUpdated.CompanyName = contr.CompanyName;
                contractorToBeUpdated.LineOfBusiness = contr.LineOfBusiness;
                context.SaveChanges();
                return UpdateContractorResult.ContractorAddesSuccessfully;
            }
        }

        public async Task<bool> DeleteContractor(int id)
        {
            using(var context = new ReportedIssuesEntities())
            {
                var contractorToBeRemoved = context.Contractors
                    .FirstOrDefault(x => x.ContractorID == id);

                if (contractorToBeRemoved!=null)
                {
                    context.Contractors.Remove(contractorToBeRemoved);
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }
    }
}