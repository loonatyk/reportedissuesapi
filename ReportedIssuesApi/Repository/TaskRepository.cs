﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using ReportedIssuesApi.Models;

namespace ReportedIssuesApi.Repository
{
    public class TaskRepository
    {
        //ALL
        public async Task<List<TaskModel>> GetAllTasks()
        {
            using (var context = new ReportedIssuesEntities())
            {
                var tasks = context.Tasks
                    .Select(x => new TaskModel()
                    {
                        TaskID = x.TaskID,
                        Lat = x.Lat,
                        Lng = x.Lng,
                        Description = x.Description,
                        Status = x.Status,
                        Address = x.Adres,
                        TaskDate = (DateTime)x.TaskDate
                    })
                .ToList<TaskModel>();
                if (tasks.Count != 0)
                {
                    return tasks;
                }
                else
                {
                    return null;
                }
            }
        }

        //BY ID
        public async Task<List<TaskModel>> GetTaskByID(int id)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var task = context.Tasks
                    .Where(x => x.TaskID == id)
                    .Select(x => new TaskModel()
                    {
                        TaskID = x.TaskID,
                        Lat = x.Lat,
                        Lng = x.Lng,
                        Description = x.Description,
                        Status = x.Status,
                        Address = x.Adres,
                        TaskDate = (DateTime)x.TaskDate
                    })
                .ToList<TaskModel>();
                if (task.Count != 0)
                {
                    return task;
                }
                else
                {
                    return null;
                }
            }
        }

        //BY STATUS
        public async Task<List<TaskModel>> GetTaskByStatus(string status)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var tasks = context.Tasks
                    .Where(x => x.Status == status)
                    .Select(x => new TaskModel()
                    {
                        TaskID = x.TaskID,
                        Lat = x.Lat,
                        Lng = x.Lng,
                        Description = x.Description,
                        Address = x.Adres,
                        TaskDate = (DateTime)x.TaskDate
                    })
                .ToList<TaskModel>();
                if (tasks.Count != 0)
                {
                    return tasks;
                }
                else
                {
                    return null;
                }
            }
        }

        //Update STATUS
        public async Task<bool> UpdateStatus(int id, string status)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var task = context.Tasks
                    .FirstOrDefault(x => x.TaskID == id);
                if (task != null)
                {
                    task.Status = status;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //Update TASK
        public async Task<bool> UpdateTask(int id, TaskModel taskUpDated)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var task = context.Tasks
                    .FirstOrDefault(x => x.TaskID == id);
                if (task != null)
                {
                    task.Lat = taskUpDated.Lat;
                    task.Lng = taskUpDated.Lng;
                    task.Description = taskUpDated.Description;
                    task.Adres = taskUpDated.Address;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //INSERT
        Models.Task insertedTask = new Models.Task();
        public async Task<bool> InsertTask(TaskModel newTask)
        {
            using (var context = new ReportedIssuesEntities())
            {
                if (newTask == null)
                {
                    return false;
                }

                insertedTask.Lat = newTask.Lat;
                insertedTask.Lng = newTask.Lng;
                insertedTask.Description = newTask.Description;
                insertedTask.Adres = newTask.Address;
                insertedTask.Status = "New";
                context.Tasks.Add(insertedTask);
                context.SaveChanges();
                return true;
            }
        }

        //DELETE
        public async Task<bool> DeleteTask(int id)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var task = context.Tasks
                    .Where(x => x.TaskID == id)
                    .FirstOrDefault();
                if (task != null)
                {
                    context.Tasks.Remove(task);
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}