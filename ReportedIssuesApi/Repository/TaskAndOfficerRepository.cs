﻿using ReportedIssuesApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ReportedIssuesApi.Repository
{
    public class TaskAndOfficerRepository
    {

        public async Task<List<TaskAndOfficerModel>> GetAllTaskAndOfficer()
        {
            using (var context = new ReportedIssuesEntities())
            {
                var all = context.TaskAndOfficers
                    .Select(x => new TaskAndOfficerModel
                    {
                        TaskAndOfficerID = x.TaskAndOfficerID,
                        TaskID = x.TaskID,
                        OfficerID = x.UserID
                    }).ToList<TaskAndOfficerModel>();

                if (all.Count == 0)
                {
                    return null;
                }
                else
                {
                    return all;
                }
            }
        }

        public async Task<List<TaskAndOfficerModel>> GetAllByTaskID(int id)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var all = context.TaskAndOfficers
                    .Where(x=>x.TaskID==id)
                    .Select(x => new TaskAndOfficerModel
                    {
                        TaskAndOfficerID = x.TaskAndOfficerID,
                        TaskID = x.TaskID,
                        OfficerID = x.UserID
                    }).ToList<TaskAndOfficerModel>();

                if (all.Count == 0)
                {
                    return null;
                }
                else
                {
                    return all;
                }
            }
        }

        public async Task<List<TaskAndOfficerModel>> GetAllByOfficerID(int id)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var all = context.TaskAndOfficers
                    .Where(x=>x.UserID==id)
                    .Select(x => new TaskAndOfficerModel
                    {
                        TaskAndOfficerID = x.TaskAndOfficerID,
                        TaskID = x.TaskID,
                        OfficerID = x.UserID
                    }).ToList<TaskAndOfficerModel>();

                if (all.Count == 0)
                {
                    return null;
                }
                else
                {
                    return all;
                }
            }
        }

        public async Task<bool> InsertTaskAndOfficer(TaskAndOfficerModel taskAndOfficer)
        {
            if (taskAndOfficer == null)
            {
                return false;
            }

            using (var context = new ReportedIssuesEntities())
            {
                var task = context.Tasks.Where(x => x.TaskID == taskAndOfficer.TaskID)
                   .FirstOrDefault();
                //.Select(x => new TaskModel()
                //{
                //    TaskID = x.TaskID
                //}).ToList<TaskModel>();
                var user = context.Users.Where(x => x.UserID == taskAndOfficer.OfficerID)
                    .FirstOrDefault();
                    //.Select(x => new UserModel()
                    //{
                    //    UserID = x.UserID
                    //}).ToList<UserModel>();

                if (task == null || user == null)
                {
                    return false;
                }
                else
                {
                    TaskAndOfficer taskandofficer = new TaskAndOfficer();
                    taskandofficer.TaskID = taskAndOfficer.TaskID;
                    taskandofficer.UserID = taskAndOfficer.OfficerID;
                    context.TaskAndOfficers.Add(taskandofficer);
                    context.SaveChanges();
                    return true;
                }
            }
        }




        public async Task<bool> UpdateTaskAndOfficer(TaskAndOfficerModel updateTaskAdnOfficer)
        {
            if (updateTaskAdnOfficer == null)
            {
                return false;
            }
            using (var context = new ReportedIssuesEntities())
            {
                var isExist = context.TaskAndOfficers
                    .Where(x => x.TaskID == updateTaskAdnOfficer.TaskID && x.UserID == x.UserID)
                    .Select(x => new TaskAndOfficer()
                    {
                        TaskAndOfficerID = x.TaskAndOfficerID
                    }).ToList();
                if (isExist == null)
                {
                    var task = context.TaskAndOfficers
                        .FirstOrDefault(x => x.TaskAndOfficerID == updateTaskAdnOfficer.TaskAndOfficerID);
                    task.TaskID = updateTaskAdnOfficer.TaskID;
                    task.UserID = updateTaskAdnOfficer.OfficerID;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public async Task<bool> DeleteTaskAndOfficer(int id)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var task = context.TaskAndOfficers
                    .FirstOrDefault(x => x.TaskAndOfficerID == id);

                if(task!=null)
                {
                    context.TaskAndOfficers.Remove(task);
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}



